$(document).ready(onReady);

function onReady() {
	$("#ejer-1 button").click(desaparecer);
	$("#ejer-2 button").click(desaparecer2);
	$("#ejer-3 #img-1").click(ejer31);
	$("#ejer-3 #img-2").click(ejer32);
	$("#ejer-3 #img-3").click(ejer33);
	$("#ejer-4 button").click(animarBox);
	$("#ejer-5").click(hrefA);
	$("#ejer-6 .btn-A").click(btA);
	$("#ejer-6 .btn-B").click(btB);

}

function desaparecer() {
	$("#ejer-1").hide();
}

function desaparecer2() {
	$('ul').eq(1).find('li').first().hide();
}


function ejer31() {
$(this).css("opacity","0.5")
};


function ejer32() {
$(this).animate({
    opacity: 0.5
  }, 5000, function() {
  });
};

function ejer33() {
$(this).animate({
    opacity: 0.5
  }, 500, function() {
  });
};


function animarBox() {
	$(".box").css({"width":"50px","height":"50px", "background-color":"orange", "position":"relative"}).animate({
    left: "250px",
    width: "150px",
    height:"150px"
  }, 4000, "easeInOutBounce" ,function() {
  });
}

function hrefA() {
	$("#ejer-5 a").html('<a href="http://www.google.es/intl/es/earth/index.html" title="google">Google Earth</a>')
}

function btA() {
	$("#ejer-6 ul").append("<li>Texto Añadido A</li>");
}


function btB() {
	$("#ejer-6 ul").append("<li>Texto Añadido B</li>");
}